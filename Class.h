#ifndef CLASS_H_
#define CLASS_H_

class Field;
#include <string>
#include <list>
#include <map>
#include "Field.h"
#include "Method.h"
#include "Auxfile.h"
#include "Exceptions.h"
class Object;

class Class {
private:
	Class* _baseClass;
	string _name;
	std::map<string, Method> _methods;
	std::map<string, Field> _instanceFields;
	std::map<string, Field>* _staticFields;
	int* _staticFieldPointers;
	std::list<Object*> _instances;
	Object* _runningObj;
public:
	Class(Class* base, std::string name) :
			_baseClass(base), _name(name), _staticFieldPointers(new int), _staticFields(
					new std::map<string, Field>()), _runningObj(
			NULL) {
		// copy values of static fields
		if (base != NULL) {
			delete _staticFieldPointers;
			delete _staticFields;
			this->_staticFields = base->_staticFields;
			this->_staticFieldPointers = base->_staticFieldPointers;
			this->_staticFields = base->_staticFields;
			(*_staticFieldPointers)++;
		} else {
			(*_staticFieldPointers) = 1;
		}
	}

	~Class() {
		(*_staticFieldPointers) = (*_staticFieldPointers) - 1;
		if ((*_staticFieldPointers) <= 0) {
			delete _staticFields;
			delete _staticFieldPointers;
		}
	}
	Object* newInstance();

	void addMethod(std::string name, Func func);

	void addInstanceField(std::string name, Type t);

	void addStaticField(std::string name, Type t);

	Field getField(std::string name);

	std::list<Field> getFields();

	Method getMethod(std::string name);

	std::list<Method> getMethods();

	int getInt(std::string name);

	void setInt(std::string name, int value);

	Object* getObj(std::string name);

	void setObj(std::string name, Object* value);

	bool canUnderstandMethod(std::string name);

	bool canUnderstandField(std::string name);

	Class* getSuperClass();

	string name() {
		return _name;
	}

	void updateCurrentInvokeObj(Object* obj);

	Object* currentlyRunning() {
		return _runningObj;
	}

	static void setAccessible(bool s);

	bool isAccessible() {
		return Aux::s;
	}

	void checkField(string name,Type withType);

	bool isStaticField(string name);

	bool isSonOfClass(string name);
};


#endif /* CLASS_H_ */
