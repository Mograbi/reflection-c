/*
 * Class.cpp
 *
 *  Created on: Jan 15, 2016
 *      Author: Moawiya
 */

#include "Class.h"
#include <exception>

Object* Class::newInstance() {
	Class* c = this;
	Object* obj = new Object(c);
	while (c != NULL) {
		typename std::map<string, Field>::iterator iter =
				c->_instanceFields.begin();
		for (; iter != c->_instanceFields.end(); ++iter) {
			Field f = iter->second;
			if (!obj->respondsToField(f.name())) {
				f.reset();
				obj->addField(f);
			}
		}
		c = c->getSuperClass();
	}
	typename std::map<string, Method>::iterator iter2 = this->_methods.begin();
	for (; iter2 != this->_methods.end(); ++iter2) {
		if (!obj->respondsToMethod(iter2->first)) {
			obj->addMethod(iter2->second);
		}
	}
	this->_instances.insert(_instances.end(), obj);
	return obj;
}

void Class::addMethod(std::string name, Func func) {
	Method m(name, this->_name, func);
	_methods.insert(std::pair<string, Method>(name, m));
}

void Class::addInstanceField(std::string name, Type t) {
	Field f(name, this->_name, t);
	_instanceFields.insert(std::pair<string, Field>(name, f));
}

void Class::addStaticField(std::string name, Type t) {
	Field f(name, this->_name, t);
	_staticFields->insert(std::pair<string, Field>(name, f));
}

Field Class::getField(std::string name) {
	Class* c = this;
	while (c != NULL) {
		if (c->_instanceFields.find(name) != c->_instanceFields.end()) {
			return c->_instanceFields.at(name);
		}
		c = c->getSuperClass();
	}
	if (_staticFields != nullptr)
		if (this->_staticFields->find(name) != this->_staticFields->end()) {
			return this->_staticFields->at(name);
		}
	throw FieldNotFound();
}

std::list<Field> Class::getFields() {
	Class* c = this;
	std::list<Field> l;
	while (c != NULL) {
		typename std::map<string, Field>::iterator iter =
				c->_instanceFields.begin();
		for (; iter != c->_instanceFields.end(); ++iter) {
			Field f = iter->second;
			l.insert(l.end(), f);
		}

		c = c->getSuperClass();
	}
	if (_staticFields != nullptr) {
		typename std::map<string, Field>::iterator iter2 =
				this->_staticFields->begin();
		for (; iter2 != this->_staticFields->end(); ++iter2) {
			Field f = iter2->second;
			l.insert(l.end(), f);
		}
	}
	return l;
}

Method Class::getMethod(std::string name) {
	Class* c = this;
	while (c != NULL) {
		if (c->_methods.find(name) != c->_methods.end()) {
			return c->_methods.at(name);
		}
		c = c->getSuperClass();
	}
	throw MethodNotFound();
}

std::list<Method> Class::getMethods() {
	Class* c = this;
	std::list<Method> l;
	while (c != NULL) {
		typename std::map<string, Method>::iterator iter = c->_methods.begin();
		for (; iter != c->_methods.end(); ++iter) {
			Method m = iter->second;
			l.insert(l.end(), m);
		}

		c = c->getSuperClass();
	}
	return l;
}

void Class::checkField(string name, Type withType) {
	if (_staticFields != nullptr) {
		if (_staticFields->find(name) == _staticFields->end()) {
			throw FieldNotFound();
		}
	} else {
		throw FieldNotFound();
	}
	if (_staticFields->at(name)._type != withType) {
		throw TypeError();
	}
}

int Class::getInt(std::string name) {
	checkField(name, INT);
	return _staticFields->at(name)._intValue;
}

void Class::setInt(std::string name, int value) {
	checkField(name, INT);
	Field& f = _staticFields->at(name);
	f._intValue = value;
}

Object* Class::getObj(std::string name) {
	checkField(name, OBJECT);
	return _staticFields->at(name)._objValue;
}

void Class::setObj(std::string name, Object* value) {
	checkField(name, OBJECT);
	Field& f = _staticFields->at(name);
	f._objValue = value;
}

bool Class::canUnderstandMethod(std::string name) {
	Class* c = this;
	while (c != NULL) {
		if (c->_methods.find(name) != c->_methods.end()) {
			return true;
		}
		c = c->getSuperClass();
	}
	return false;
}

bool Class::canUnderstandField(std::string name) {
	Class* c = this;
	while (c != NULL) {
		if (c->_instanceFields.find(name) != c->_instanceFields.end()) {
			return true;
		}

		c = c->getSuperClass();
	}

	if (this->_staticFields != nullptr)
		if (this->_staticFields->find(name) != this->_staticFields->end()) {
			return true;
		}
	return false;
}

Class* Class::getSuperClass() {
	return _baseClass;
}

void Class::updateCurrentInvokeObj(Object* obj) {
	this->_runningObj = obj;
}

bool Aux::s = false;
void Class::setAccessible(bool s) {
	Aux a(s);
}

bool Class::isStaticField(string name) {
	if (_staticFields == nullptr) {
		return false;
	}
	return (this->_staticFields->find(name) != _staticFields->end());
}

bool Class::isSonOfClass(string name) {
	if (this->_name == name) {
		return true;
	} else if (this->_baseClass == nullptr) {
		return false;
	}
	return this->_baseClass->isSonOfClass(name);
}

