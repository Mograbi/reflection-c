/*
 * Field.cpp
 *
 *  Created on: Jan 14, 2016
 *      Author: Moawiya
 */

#include "Field.h"
#include "Exceptions.h"
#include <sstream>

Type Field::getType() {
	return _type;
}

void Field::setInt(Object* obj, int value) {
	obj->setInt(this->name(), value);
}

int Field::getInt(Object* obj) {
	return obj->getInt(this->name());
}

void Field::setObj(Object* obj, Object* value) {
	obj->setObj(this->name(), value);
}

Object* Field::getObj(Object* obj) {
	return obj->getObj(this->name());
}

