/*
 * Auxfile.h
 *
 *  Created on: Jan 14, 2016
 *      Author: Moawiya
 */

#ifndef AUXFILE_H_
#define AUXFILE_H_

#include "Object.h"
#include "Class.h"

typedef void (*Func)(Object*);
typedef enum type_t {
	INT, OBJECT
} Type;

class Aux {
public:
	static bool s;
	Aux() {
	}
	Aux(bool b) {
		s = b;
	}
};

#endif /* AUXFILE_H_ */
