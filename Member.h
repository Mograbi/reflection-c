/*
 * Member.h
 *
 *  Created on: Jan 14, 2016
 *      Author: Moawiya
 */

#ifndef MEMBER_H_
#define MEMBER_H_

#include <string>

class Member {
private:
	std::string _name;
	std::string _className;
public:
	Member(std::string name,std::string className);

	virtual ~Member();

	std::string getDeclaringClass() const;

	std::string name() const;
};

#endif /* MEMBER_H_ */
