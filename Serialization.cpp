/*
 * Serialization.cpp
 *
 *  Created on: Jan 16, 2016
 *      Author: Moawiya
 */

#include "Serialization.h"
#include "Method.h"
#include "Field.h"
#include <stdlib.h>
#include <string>
#include <sstream>
#include <exception>
#include <assert.h>
#include <iostream>

/*
 * (syntax)
 *
 * _OBJECT
 * Class Name
 * pointer to Class
 * Fields:
 * 		_FIELD
 * 		name
 * 		class name
 * 		Type
 * 		value
 * 		_ENDOFFIELD
 * 		_FIELD
 * 		name
 * 		class name
 * 		Type
 * 		value
 * 		_ENDOFFIELD
 * _EndOfObject
 */

class SerializeAux {
public:
	static int i;
	void reset() {
		i = 0;
	}
};

int SerializeAux::i = 0;

string Serialization::marshalling(Object* o, string s) {
	SerializeAux::i++;
	std::stringstream ss1;
	ss1 << SerializeAux::i;
	std::string deflat = "_OBJECT&";
	deflat += (ss1.str() + "&");
	std::stringstream ss;

	ss << static_cast<const void*>(o->getClass());
	string pointer = ss.str();
	// 1 + 2
	deflat += (o->getClass()->name() + '&' + ss.str() + '&');
	deflat += "Fields:&";
	std::list<Field> fields = o->getClass()->getFields();
	typename std::list<Field>::iterator fieldIter = fields.begin();
	for (; fieldIter != fields.end(); ++fieldIter) {
		if (o->getClass()->isStaticField(fieldIter->name())) {
			continue;
		}
		deflat += ("_FIELD&");
		deflat += (fieldIter->name() + '&' + fieldIter->getDeclaringClass()
				+ '&');
		if (fieldIter->getType() == INT) {
			// INT FIELD
			std::ostringstream ss;
			ss << o->getInt(fieldIter->name());
			deflat += ("INT&" + ss.str() + "&" + "_ENDOFFIELD&");
		} else {
			// OBJECT FIELD
			deflat += ("OBJECT&");
			if (fieldIter->getObj(o) == nullptr) {
				deflat += "NULL&_ENDOFFIELD&";
			} else {
				deflat += (marshalling(fieldIter->getObj(o), ""));
				deflat += "_ENDOFFIELD&";
			}
		}
	}

	deflat += "_ENDOFOBJECT$" + pointer + "&" + ss1.str() + "&";

	return deflat;
}

Object* Serialization::unmarshalling(string s) {
	string line;
	string s1;
	std::stringstream ss(s);
	getline(ss, line, '&');
	assert(line == "_OBJECT");
	getline(ss, line, '&');
	getline(ss, line, '&');
	string className = line;
	getline(ss, line, '&');
	string pointer = line;
	unsigned long ul = strtoul(line.c_str(), NULL, 0);
	Class* classPointer = reinterpret_cast<Class*>(ul);
	classPointer->setAccessible(true);
	Object* o = new Object(classPointer);
	getline(ss, line, '&');
	assert(line == "Fields:");
	getline(ss, line, '&');
	while (!ss.eof() && (line.find("_ENDOFOBJECT") == string::npos)) {
		classPointer->setAccessible(true);
		assert(line == "_FIELD");
		getline(ss, line, '&');
		string fieldName = line;
		getline(ss, line, '&');
		string fieldClassName = line;
		getline(ss, line, '&');
		string type = line;
		if (type == "INT") {
			getline(ss, line, '&');
			string value = line;
			Field f(fieldName, fieldClassName, INT);
			o->addField(f);
			o->setInt(fieldName, atoi(value.c_str()));
			getline(ss, line, '&');
			assert(line == "_ENDOFFIELD");
			getline(ss, line, '&');
		} else {
			Field f(fieldName, fieldClassName, OBJECT);
			o->addField(f);
			getline(ss, line, '&');
			if (line == "_OBJECT") {
				getline(ss, s1);
				Object* obj = unmarshalling("_OBJECT&" + s1);
				ss.str(s1);
				ss.clear();
				getline(ss, line, '&');
				string number = line;
				getline(ss, line, '&');
				getline(ss, line, '&');
				classPointer->setAccessible(true);
				o->setObj(fieldName, obj);
				std::size_t pos = s1.find(
						"_ENDOFOBJECT$" + line + "&" + number);
				s1 = s1.substr(pos);
				ss.str(s1);
				ss.clear();
				getline(ss, line, '&');
				getline(ss, line, '&');
				getline(ss, line, '&');
				assert(line == "_ENDOFFIELD");
				getline(ss, line, '&');
			} else {
				// NULL - nothing to do
				getline(ss, line, '&');
				assert(line == "_ENDOFFIELD");
				getline(ss, line, '&');
			}
		}
	}
	classPointer->setAccessible(false);
	return o;
}

string Serialization::serialize(Object& o) {
	SerializeAux().reset();
	std::string s = "";
	bool prev = Aux::s;
	o.getClass()->setAccessible(true);
	s += Serialization().marshalling(&o, s);
	Class::setAccessible(prev);
	return s;
}

Object* Serialization::deserialize(string s) {
	SerializeAux().reset();
	bool prev = Aux::s;
	Object* obj = Serialization().unmarshalling(s);
	Class::setAccessible(prev);
	return obj;
}

