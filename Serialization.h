#ifndef SERIALIZATION_H_
#define SERIALIZATION_H_

#include "Object.h"
#include "Auxfile.h"
#include <string>
using std::string;

class Serialization {
private:
	string marshalling(Object* o, string s);
	Object* unmarshalling(string s);
public:

	static string serialize(Object& o);
	
	static Object* deserialize(string s);
};

#endif /* SERIALIZATION_H_ */
