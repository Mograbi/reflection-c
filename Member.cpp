/*
 * Member.cpp
 *
 *  Created on: Jan 14, 2016
 *      Author: Moawiya
 */

#include "Member.h"

Member::Member(std::string name, std::string className) :
		_name(name), _className(className) {
}

Member::~Member() {
}

std::string Member::getDeclaringClass() const {
	return _className;
}

std::string Member::name() const {
	return _name;
}

