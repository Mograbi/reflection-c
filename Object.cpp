/*
 * Object.cpp
 *
 *  Created on: Jan 14, 2016
 *      Author: Moawiya
 */

#include "Object.h"
#include "Exceptions.h"
#include "Method.h"

/*****************************************************************************/
/******************  get  ||  set  *******************************************/
/*****************************************************************************/
void Object::getSetAux(string name, Type t) {
	if (getFieldType(name) != t) {
		throw TypeError();
	}
	if (!this->respondsToField(name)) {
		throw FieldNotFound();
	}
	if (!this->isAccessible(name)) {
		throw FieldNotAccessible();
	}
}

int Object::getInt(string name) {
	getSetAux(name, INT);
	if (this->getClass()->isStaticField(name)) {
		return this->getClass()->getInt(name);
	} else {
		return this->_fields.at(name)._intValue;
	}
}

void Object::setInt(string name, int value) {
	getSetAux(name, INT);
	if (this->getClass()->isStaticField(name)) {
		this->getClass()->setInt(name, value);
	} else {
		this->_fields.at(name)._intValue = value;
	}
}

Object* Object::getObj(string name) {
	getSetAux(name, OBJECT);
	if (this->getClass()->isStaticField(name)) {
		return this->getClass()->getObj(name);
	} else {
		return this->_fields.at(name)._objValue;
	}
}

void Object::setObj(string name, Object* value) {
	getSetAux(name, OBJECT);
	if (this->getClass()->isStaticField(name)) {
		this->getClass()->setObj(name, value);
	} else {
		(_fields.at(name))._objValue = value;
	}
}
/*****************************************************************************/
void Object::invokeMethod(string name) {
	if (!respondsToMethod(name)) {
		throw MethodNotFound();
	}
	Method m = this->_class->getMethod(name);
	this->_class->updateCurrentInvokeObj(this);
	try {
		m.invoke(this);
	} catch (std::exception& e) {
		this->_class->updateCurrentInvokeObj(NULL);
		throw e;
	}
	this->_class->updateCurrentInvokeObj(nullptr);
}

bool Object::isInstanceOf(string c) {
	return _class->isSonOfClass(c);
}
/*****************************************************************************/
bool Object::respondsToMethod(string name) {
	return this->_class->canUnderstandMethod(name);
}

bool Object::respondsToField(string name) {
	if (this->_fields.find(name) == _fields.end()) {
		if (!this->_class->isStaticField(name)) {
			return false;
		}
	}
	return true;
}
/*****************************************************************************/
Class* Object::getClass() {
	return _class;
}

bool Object::isAccessible(string fieldName) {
	if (this->_class->isAccessible()) {
		return true;
	}
	if (this->_class->isStaticField(fieldName)) {
		return true;
	}
	if (this->_class->currentlyRunning() != this) {
		return false;
	}
	if (!this->respondsToField(fieldName)) {
		return false;
	}
	return true;
}
/*****************************************************************************/
void Object::addMethod(Method m) {
	if (this->_methods.find(m.name()) == this->_methods.end()) {
		_methods.insert(std::pair<string, Method>(m.name(), m));
	}
}

void Object::addField(Field f) {
	if (this->_fields.find(f.name()) == this->_fields.end()) {
		_fields.insert(std::pair<string, Field>(f.name(), f));
	}
}

Type Object::getFieldType(string name) {
	return (this->getClass()->getField(name)).getType();
}

