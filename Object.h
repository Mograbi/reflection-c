#ifndef OBJECT_H_
#define OBJECT_H_

class Method;

class Class;
#include <string>
using std::string;
#include "Class.h"
#include "Field.h"
#include "Method.h"
#include "Object.h"
class Field;

class Object {
private:
	Class* _class;
	std::map<string, Method> _methods;
	std::map<string, Field> _fields;
public:

	Object(Class* c) :
			_class(c) {
	}

	int getInt(string name);

	void setInt(string name, int value);

	Object* getObj(string name);

	void setObj(string name, Object* value);

	virtual void invokeMethod(string name);

	bool isInstanceOf(string c);

	bool respondsToMethod(string name);

	bool respondsToField(string name);

	Class* getClass();

	bool isAccessible(string fieldName);

	void addMethod(Method m);
	void addField(Field f);

	Type getFieldType(string name);

	void getSetAux(string name, Type t);

};

#endif /* OBJECT_H_ */
