#ifndef FIELD_H_
#define FIELD_H_

class Object;
#include "Member.h"
#include "Object.h"
#include <stdlib.h>
#include "Auxfile.h"
#include "Class.h"

class Field: public Member {
	friend class Class;
	friend class Object;
private:
	Type _type;
	int _intValue;
	Object* _objValue;
public:
	Field(std::string name, std::string className, Type type) :
			Member(name, className), _type(type) {
		_intValue = 0;
		_objValue = NULL;
	}

	Type getType();

	void setInt(Object* obj, int value);

	int getInt(Object* obj);

	void setObj(Object* obj, Object* value);

	Object* getObj(Object* obj);

	void reset() {
		this->_intValue = 0;
		this->_objValue = NULL;
	}

};

#endif /* FIELD_H_ */
