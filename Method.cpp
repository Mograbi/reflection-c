/*
 * Method.cpp
 *
 *  Created on: Jan 14, 2016
 *      Author: Moawiya
 */


#include "Method.h"
#include "Exceptions.h"

void Method::invoke(Object* const obj) {
	if(! obj->respondsToMethod(this->name())) {
		throw MethodNotFound();
	}
	_func(obj);
}
