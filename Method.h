
#ifndef METHOD_H_
#define METHOD_H_

#include "Member.h"
#include "Object.h"
#include "Auxfile.h"

class Method : public Member{
private:
	Func _func;
public:

	Method(std::string name,std::string className,Func f) : Member(name,className) {
		_func = f;
	}

	virtual void invoke(Object* const obj);
};


#endif /* METHOD_H_ */
